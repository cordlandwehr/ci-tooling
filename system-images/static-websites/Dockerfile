FROM ubuntu:18.04

LABEL Description="KDE Static website generation image"
MAINTAINER Bhushan Shah

USER root

RUN apt update && DEBIAN_FRONTEND=noninteractive apt install --yes \
ruby \
ruby-dev \
build-essential \
libsqlite3-dev \
zlib1g-dev \
git \
python3-pip \
python3-venv \
php7.2-cli php7.2-xml \
fontforge \
wget curl rsync pandoc && apt clean

RUN curl -sS https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add - && echo "deb https://deb.nodesource.com/node_10.x bionic main" > /etc/apt/sources.list.d/nodesource.list
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list
RUN apt update && apt install --yes yarn && apt clean

RUN wget https://github.com/gohugoio/hugo/releases/download/v0.59.1/hugo_0.59.1_Linux-64bit.deb && \
	dpkg -i hugo_0.59.1_Linux-64bit.deb && rm hugo_0.59.1_Linux-64bit.deb

RUN gem update --system && gem install xdg:2.2.5 jekyll:3.8.4 jekyll-feed jekyll-theme-minimal jekyll-readme-index jekyll-relative-links jekyll-environment-variables pluto bundler haml redcarpet minima rdiscount inqlude addressable jekyll-kde-theme rouge

RUN pip3 install sphinx-intl aether-sphinx sphinx_rtd_theme sphinxcontrib-doxylink sphinxcontrib-websupport requests pytx dateparser pelican mdx_video markdown beautifulsoup4

RUN wget https://getcomposer.org/download/1.9.0/composer.phar && mv composer.phar /usr/bin/composer && chmod +x /usr/bin/composer

RUN apt-get -qq update && apt-get install -qq \
    openssh-server \
    openjdk-8-jre-headless \
    && dpkg-reconfigure openssh-server && mkdir -p /var/run/sshd && apt-get -qq clean

# Setup a user account for everything else to be done under
RUN useradd -d /home/user/ -u 1000 --user-group --create-home -G video user

EXPOSE 22
CMD ["/usr/sbin/sshd", "-D"]
